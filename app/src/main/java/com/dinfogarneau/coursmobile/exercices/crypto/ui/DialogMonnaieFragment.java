package com.dinfogarneau.coursmobile.exercices.crypto.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;

import com.dinfogarneau.coursmobile.exercices.crypto.model.Crypto;
import com.dinfogarneau.coursmobile.exercices.crypto.R;

import java.util.zip.Inflater;


public class DialogMonnaieFragment extends DialogFragment {

    private static final String MODIFIER_TITLE = "Modifier la crypto";
    private static final String AJOUTER_TITLE = "Ajouter la crypto";

    private static final String MODIFIER = "Modifier";
    private static final String AJOUTER = "Ajouter";
    private static final String ANNULER = "Annuler";
    private static final String ERREUR = "Erreur dans la création de la crypto";

    private Crypto crypto1;
    private int position = -1;
    private boolean posseder = false;

    public interface EditCryptoDialogListener {
        void onFinishEditDialog(Crypto crypto, int position);
    }

    public DialogMonnaieFragment() {
    }

    public DialogMonnaieFragment(Crypto crypto, int position) {
        this.crypto1 = crypto;
        this.position = position;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String formButton = "";
        if (this.position == -1) {
            formButton = AJOUTER;
        } else {
            formButton = MODIFIER;
        }

        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        final View v = inflater.inflate(R.layout.monnaies_form, null);
        final Context context = getContext();

        if (crypto1 != null) {
            EditText et_nom = v.findViewById(R.id.et_nom_monnaie);
            EditText et_symbole = v.findViewById(R.id.et_sym_mon);
            EditText et_prix = v.findViewById(R.id.et_prix_mon);
            EditText et_quantite = v.findViewById(R.id.et_quan_mon);
            et_nom.setText(crypto1.getNom());
            et_symbole.setText(crypto1.getSymbole());
            et_prix.setText(Integer.toString(crypto1.getPrix()));
            et_quantite.setText(Double.toString(crypto1.getQuantite()));
            this.posseder = crypto1.isPosseder();
        }

        builder.setView(v)
                .setPositiveButton(formButton, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        EditText et_nom = v.findViewById(R.id.et_nom_monnaie);
                        EditText et_symbole = v.findViewById(R.id.et_sym_mon);
                        EditText et_prix = v.findViewById(R.id.et_prix_mon);
                        EditText et_quantite = v.findViewById(R.id.et_quan_mon);


                        String nom = et_nom.getText().toString().trim();
                        String symbole = et_symbole.getText().toString().trim();
                        int prix = Integer.parseInt(et_prix.getText().toString());
                        double quantite = Double.parseDouble(et_quantite.getText().toString());

                        if (nom.isEmpty() || symbole.isEmpty() || prix < 0 || quantite < 0) {
                            Toast.makeText(context, ERREUR, Toast.LENGTH_SHORT).show();
                            return;
                        }

                        Crypto crypto = new Crypto(nom, symbole, prix, quantite, posseder);
                        if (crypto1 != null) {
                            crypto.setId(crypto1.getId());
                        }
                        EditCryptoDialogListener listener = (EditCryptoDialogListener) getActivity();
                        listener.onFinishEditDialog(crypto, position);
                    }

                })
                .setNegativeButton(ANNULER, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        // Create the AlertDialog object and return it
        if (position > -1) {
            builder.setTitle(MODIFIER_TITLE);
        } else {
            builder.setTitle(AJOUTER_TITLE);
        }
        return builder.create();
    }
}
