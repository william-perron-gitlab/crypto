package com.dinfogarneau.coursmobile.exercices.crypto.ui;

import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dinfogarneau.coursmobile.exercices.crypto.R;
import com.dinfogarneau.coursmobile.exercices.crypto.model.Crypto;

import java.util.List;

public class PortefeuilleListAdapter extends RecyclerView.Adapter<PortefeuilleListAdapter.PortefeuilleViewHolder>{

    private List<Crypto> portefeuilleList;
    private onItemClickListener mListener;

    public void setPortefeuilleList(List<Crypto> portefeuille) {
        portefeuilleList = portefeuille;
        notifyDataSetChanged();
    }

    public List<Crypto> getPortefeuilleList() { return portefeuilleList; }

    public interface onItemClickListener {
        void supprimerMonnaie(Crypto crypto);

        void editerMonnaie(int position);
    }

    public void setOnClickListener(PortefeuilleListAdapter.onItemClickListener listener) {
        this.mListener = listener;
    }

    @NonNull
    @Override
    public PortefeuilleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.portefeuille_item, parent, false);
        return new PortefeuilleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PortefeuilleViewHolder portefeuilleViewHolder, int position) {

        if (portefeuilleList != null) {
            Crypto current = portefeuilleList.get(position);
            portefeuilleViewHolder.tvPorNom.setText(current.getNom());
            portefeuilleViewHolder.tvPorSym.setText(current.getSymbole());
            portefeuilleViewHolder.tvPorPrix.setText(formatPrice("" + current.getPrix()));
            portefeuilleViewHolder.tvPorQuan.setText("" + current.getQuantite());
            portefeuilleViewHolder.tvPorTotalAction.setText(formatTotalAction(current.getQuantite(), current.getPrix()));
            //portefeuilleViewHolder.tvPorTotal.setText();
            int imageId = portefeuilleViewHolder.itemView.getContext().getResources().getIdentifier(current.getSymbole(), "drawable", portefeuilleViewHolder.itemView.getContext().getPackageName());
            portefeuilleViewHolder.ivPor.setImageResource(imageId);
        } else {
            portefeuilleViewHolder.tvPorNom.setText("Pas de todo !");
        }
    }

    private String formatPrice(String price) {
        int lenght = price.length();
        return price.substring(0, lenght-2) + "." + price.substring(lenght-2) + "$";
    }

    private String formatTotalAction(double quantite, int prix) {
        return (prix / 100) * quantite + "$";
    }

    @Override
    public int getItemCount() {
        if (portefeuilleList != null)
            return portefeuilleList.size();
        else return 0;
    }

    public class PortefeuilleViewHolder extends RecyclerView.ViewHolder {
        public TextView tvPorNom;
        public TextView tvPorSym;
        public TextView tvPorPrix;
        public TextView tvPorQuan;
        public TextView tvPorTotalAction;
        public ImageView ivPor;
        public TextView tvPorTotal;

        public PortefeuilleViewHolder(@NonNull View itemView) {
            super(itemView);
            tvPorNom = itemView.findViewById(R.id.tv_por_nom);
            tvPorSym = itemView.findViewById(R.id.tv_por_sym);
            tvPorPrix = itemView.findViewById(R.id.tv_por_prix);
            tvPorQuan = itemView.findViewById(R.id.tv_por_quan);
            tvPorTotalAction = itemView.findViewById(R.id.tv_por_total);
            ivPor = itemView.findViewById(R.id.iv_por);
            tvPorTotal = itemView.findViewById(R.id.tv_total_amount);

            itemView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
                @Override
                public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                    MenuItem edit = menu.add(0, v.getId(), 0, R.string.modifier);
                    MenuItem delete = menu.add(0, v.getId(), 1, R.string.supp);

                    delete.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            int position = getAdapterPosition();
                            Crypto crypto = portefeuilleList.get(position);
                            mListener.supprimerMonnaie(crypto);
                            return false;
                        }
                    });

                    edit.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            int position = getAdapterPosition();
                            mListener.editerMonnaie(position);
                            return false;
                        }
                    });
                }
            });
        }
    }
}
