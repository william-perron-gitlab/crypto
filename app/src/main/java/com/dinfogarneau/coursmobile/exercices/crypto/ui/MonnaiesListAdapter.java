package com.dinfogarneau.coursmobile.exercices.crypto.ui;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dinfogarneau.coursmobile.exercices.crypto.R;
import com.dinfogarneau.coursmobile.exercices.crypto.model.Crypto;

import java.time.format.ResolverStyle;
import java.util.List;

public class MonnaiesListAdapter extends RecyclerView.Adapter<MonnaiesListAdapter.MonnaiesViewHolder>{

    private List<Crypto> monnaiesList;
    private onItemClickListener mListener;

    public void setMonnaiesList(List<Crypto> monnaies) {
        monnaiesList = monnaies;
    }

    public List<Crypto> getMonnaiesList() { return monnaiesList; }

    public interface onItemClickListener {
        void ajouterPortefeuille(Crypto crypto);

        void supprimerMonnaie(Crypto crypto);

        void editerMonnaie(int position);
    }

    public void setOnClickListener(onItemClickListener listener) {
        this.mListener = listener;
    }

    @NonNull
    @Override
    public MonnaiesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.monnaies_item, parent, false);
        return new MonnaiesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MonnaiesViewHolder monnaiesViewHolder, int position) {

        if (monnaiesList != null) {
            Crypto current = monnaiesList.get(position);
            monnaiesViewHolder.tvMonnaieNom.setText(current.getNom());
            monnaiesViewHolder.tvMonnaieSymbole.setText(current.getSymbole());
            monnaiesViewHolder.tvMonnaiePrix.setText(formatPrice("" + current.getPrix()));
            int imageId = monnaiesViewHolder.itemView.getContext().getResources().getIdentifier(current.getSymbole(), "drawable", monnaiesViewHolder.itemView.getContext().getPackageName());
            monnaiesViewHolder.ivMonnaie.setImageResource(imageId);
        } else {
            monnaiesViewHolder.tvMonnaieNom.setText("Pas de crypto !");
        }
    }

    private String formatPrice(String price) {
        int lenght = price.length();
        return price.substring(0, lenght-2) + "." + price.substring(lenght-2) + "$";
    }

    @Override
    public int getItemCount() {
        if (monnaiesList != null)
            return monnaiesList.size();
        else return 0;
    }

    public class MonnaiesViewHolder extends RecyclerView.ViewHolder {
        public TextView tvMonnaieNom;
        public TextView tvMonnaieSymbole;
        public TextView tvMonnaiePrix;
        public ImageView ivMonnaie;

        public MonnaiesViewHolder(@NonNull View itemView) {
            super(itemView);
            tvMonnaieNom = itemView.findViewById(R.id.tv_mon_nom);
            tvMonnaieSymbole = itemView.findViewById(R.id.tv_mon_sym);
            tvMonnaiePrix = itemView.findViewById(R.id.tv_mon_prix);
            ivMonnaie = itemView.findViewById(R.id.iv_mon);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        int position = getAdapterPosition();
                        // Bonne pratique pour s'assurer que l'élément clické est toujours présent
                        if (position != RecyclerView.NO_POSITION) {
                            Crypto crypto = monnaiesList.get(position);
                            mListener.ajouterPortefeuille(crypto);
                        }
                    }
                }
            });

            itemView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
                @Override
                public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                    MenuItem edit = menu.add(0, v.getId(), 0, R.string.modifier);
                    MenuItem delete = menu.add(0, v.getId(), 1, R.string.supp);

                    delete.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            int position = getAdapterPosition();
                            Crypto crypto = monnaiesList.get(position);
                            mListener.supprimerMonnaie(crypto);
                            return false;
                        }
                    });

                    edit.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            int position = getAdapterPosition();
                            mListener.editerMonnaie(position);
                            return false;
                        }
                    });
                }
            });
        }
    }
}
