package com.dinfogarneau.coursmobile.exercices.crypto.ui;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.dinfogarneau.coursmobile.exercices.crypto.data.AppExecutors;
import com.dinfogarneau.coursmobile.exercices.crypto.data.CryptoRoomDatabase;
import com.dinfogarneau.coursmobile.exercices.crypto.model.Crypto;

import java.util.List;

public class MonnaieViewModel extends AndroidViewModel {

    private LiveData<List<Crypto>> cryptos;
    private CryptoRoomDatabase mdb;

    public MonnaieViewModel(@NonNull Application application) {
        super(application);
        mdb = CryptoRoomDatabase.getDatabase(application);
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
               cryptos = mdb.cryptoDAO().getAllLiveCrypto();
            }
        });
    }

    public LiveData<List<Crypto>> getCryptos() {
        return cryptos;
    }
}
