package com.dinfogarneau.coursmobile.exercices.crypto.data;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.dinfogarneau.coursmobile.exercices.crypto.model.Crypto;

import java.util.List;

@Dao
public interface CryptoDAO {

    @Insert
    void insert(Crypto crypto);

    @Update
    void updateCrypto(Crypto... crypto);

    @Delete
    void delete(Crypto crypto);

    @Query("DELETE FROM crypto")
    void deleteAll();

    @Query("SELECT * FROM crypto")
    List<Crypto> getAllMonnaiesCrypto();

    @Query("SELECT * FROM crypto WHERE posseder = 1")
    List<Crypto> getAllPortefeuileCrypto();

    @Query("SELECT * FROM crypto ORDER BY nom ASC")
    LiveData<List<Crypto>> getAllLiveCrypto();

    @Query("SELECT * FROM crypto WHERE posseder = 1 ORDER BY nom ASC")
    LiveData<List<Crypto>> getAllLivePortefeuilleCrypto();

    @Query("UPDATE crypto SET posseder=0 WHERE id = :id")
    void supprimerPortefeuille(int id);
}
