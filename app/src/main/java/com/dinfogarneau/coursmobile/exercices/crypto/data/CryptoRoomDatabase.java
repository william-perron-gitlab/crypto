package com.dinfogarneau.coursmobile.exercices.crypto.data;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.dinfogarneau.coursmobile.exercices.crypto.model.Crypto;

@Database(entities = {Crypto.class}, version = 1)
public abstract class CryptoRoomDatabase extends RoomDatabase {

    public static volatile CryptoRoomDatabase INSTANCE;

    public abstract CryptoDAO cryptoDAO();

    public static synchronized CryptoRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            // Crée la BDD
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                    CryptoRoomDatabase.class, "crypto_database")
                    .build();
        }
        return INSTANCE;
    }
}
