package com.dinfogarneau.coursmobile.exercices.crypto.ui;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.dinfogarneau.coursmobile.exercices.crypto.data.AppExecutors;
import com.dinfogarneau.coursmobile.exercices.crypto.data.CryptoRoomDatabase;
import com.dinfogarneau.coursmobile.exercices.crypto.model.Crypto;

import java.util.List;

public class PortefeuilleViewModel extends AndroidViewModel {
    private LiveData<List<Crypto>> cryptos;
    private CryptoRoomDatabase mdb;

    public PortefeuilleViewModel(@NonNull Application application) {
        super(application);
        mdb = CryptoRoomDatabase.getDatabase(application);
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                cryptos = mdb.cryptoDAO().getAllLivePortefeuilleCrypto();
            }
        });
    }

    public LiveData<List<Crypto>> getCryptos() {
        return cryptos;
    }
}
