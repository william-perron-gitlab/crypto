package com.dinfogarneau.coursmobile.exercices.crypto.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "crypto")
public class Crypto {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @NonNull
    @ColumnInfo(name="nom")
    private String nom;

    @NonNull
    @ColumnInfo(name="symbole")
    private String symbole;

    @NonNull
    @ColumnInfo(name="prix")
    private int prix;

    @NonNull
    @ColumnInfo(name="quantite")
    private double quantite;

    @NonNull
    @ColumnInfo(name="posseder")
    private boolean posseder;

    public Crypto(String nom, String symbole, int prix, double quantite, boolean posseder) {
        this.nom = nom;
        this.symbole = symbole;
        this.prix = prix;
        this.quantite = quantite;
        this.posseder = posseder;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getSymbole() {
        return symbole;
    }

    public void setSymbole(String symbole) {
        this.symbole = symbole;
    }

    public int getPrix() {
        return prix;
    }

    public void setPrix(int prix) {
        this.prix = prix;
    }

    public double getQuantite() {
        return quantite;
    }

    public void setQuantite(double quantite) {
        this.quantite = quantite;
    }

    public boolean isPosseder() {
        return posseder;
    }

    public void setPosseder(boolean posseder) {
        this.posseder = posseder;
    }
}
