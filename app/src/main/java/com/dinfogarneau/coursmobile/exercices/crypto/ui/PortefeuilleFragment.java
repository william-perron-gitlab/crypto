package com.dinfogarneau.coursmobile.exercices.crypto.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toolbar;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.dinfogarneau.coursmobile.exercices.crypto.R;
import com.dinfogarneau.coursmobile.exercices.crypto.data.AppExecutors;
import com.dinfogarneau.coursmobile.exercices.crypto.data.CryptoRoomDatabase;
import com.dinfogarneau.coursmobile.exercices.crypto.model.Crypto;
import java.util.List;

public class PortefeuilleFragment extends Fragment {

    private static final String PORTEFEUILLE = "Portefeuille";

    private CryptoRoomDatabase mDb;
    private PortefeuilleViewModel portefeuilleViewModel;
    private Button btAddMonnaie;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        getActivity().setTitle(PORTEFEUILLE);
        portefeuilleViewModel = new ViewModelProvider(this).get(PortefeuilleViewModel.class);
        return inflater.inflate(R.layout.fragment_portefeuille, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.bt_por_monnaies).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(PortefeuilleFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });

        mDb = CryptoRoomDatabase.getDatabase(getActivity());

        RecyclerView recyclerView = view.findViewById(R.id.rv_portefeuile);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
        PortefeuilleListAdapter portefeuilleListAdapter = new PortefeuilleListAdapter();
        recyclerView.setAdapter(portefeuilleListAdapter);

        portefeuilleListAdapter.setOnClickListener(new PortefeuilleListAdapter.onItemClickListener() {
            @Override
            public void supprimerMonnaie(Crypto crypto) {
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        mDb.cryptoDAO().supprimerPortefeuille(crypto.getId());
                    }
                });
            }

            @Override
            public void editerMonnaie(int position) {
                DialogMonnaieFragment newFragment = new DialogMonnaieFragment(portefeuilleListAdapter.getPortefeuilleList().get(position), position);
                newFragment.show(getActivity().getSupportFragmentManager(), "Modifier crypto");
            }
        });

//        AppExecutors.getInstance().diskIO().execute(new Runnable() {
//            @Override
//            public void run() {
//                List<Crypto> cryptos = mDb.cryptoDAO().getAllPortefeuileCrypto();
//                getActivity().runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        portefeuilleListAdapter.setPortefeuilleList(cryptos);
//                        portefeuilleListAdapter.notifyDataSetChanged();
//                    }
//                });
//            }
//        });

        portefeuilleViewModel.getCryptos().observe(getViewLifecycleOwner(), new Observer<List<Crypto>>() {
            @Override
            public void onChanged(List<Crypto> cryptos) {
                portefeuilleListAdapter.setPortefeuilleList(cryptos);
                portefeuilleListAdapter.notifyDataSetChanged();
            }
        });
    }
}