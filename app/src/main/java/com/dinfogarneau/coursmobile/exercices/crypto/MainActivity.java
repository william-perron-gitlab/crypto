package com.dinfogarneau.coursmobile.exercices.crypto;

import android.os.Bundle;

import com.dinfogarneau.coursmobile.exercices.crypto.data.CryptoRoomDatabase;
import com.dinfogarneau.coursmobile.exercices.crypto.model.Crypto;
import com.dinfogarneau.coursmobile.exercices.crypto.ui.DialogMonnaieFragment;
import com.dinfogarneau.coursmobile.exercices.crypto.ui.MonnaiesFragment;
import com.dinfogarneau.coursmobile.exercices.crypto.ui.MonnaiesListAdapter;
import com.dinfogarneau.coursmobile.exercices.crypto.ui.PortefeuilleFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;

import com.dinfogarneau.coursmobile.exercices.crypto.data.AppExecutors;

import android.util.Log;
import android.view.MenuInflater;
import android.view.View;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

import java.util.List;

public class MainActivity extends AppCompatActivity implements DialogMonnaieFragment.EditCryptoDialogListener {
    private CryptoRoomDatabase mDb;
    private FloatingActionButton btAjouter;
    private RecyclerView monnaiesList;
    private MonnaiesFragment monnaieFragment;
    private PortefeuilleFragment portefeuilleFragment;
    private MonnaiesListAdapter monnaiesListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.tb_haut);
        setSupportActionBar(toolbar);

        mDb = CryptoRoomDatabase.getDatabase(getApplicationContext());
        btAjouter = findViewById(R.id.bt_ajouter);
        btAjouter.setVisibility(View.INVISIBLE);
        monnaiesList = findViewById(R.id.rv_monnaies);

        monnaieFragment = (MonnaiesFragment) getSupportFragmentManager().findFragmentById(R.id.first_fragment);
        portefeuilleFragment = (PortefeuilleFragment) getSupportFragmentManager().findFragmentById(R.id.second_fragment);

        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                mDb.cryptoDAO().deleteAll();
                Crypto crypto1 = new Crypto("Bitcoin","btc", 50000, 10, false);
                mDb.cryptoDAO().insert(crypto1);
                Crypto crypto2 = new Crypto("Dogecoin","doge", 40000, 10, false);
                mDb.cryptoDAO().insert(crypto2);
                Crypto crypto3 = new Crypto("XLM","xlm", 40500, 10, false);
                mDb.cryptoDAO().insert(crypto3);
            }
        });

        btAjouter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogMonnaieFragment formAjouter = new DialogMonnaieFragment();
                formAjouter.show(getSupportFragmentManager(), "Ajouter");
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()) {
            case R.id.menu_admin:
                if (btAjouter.getVisibility() == View.VISIBLE) {
                    btAjouter.setVisibility(View.INVISIBLE);
                } else {
                    btAjouter.setVisibility(View.VISIBLE);
                }
                return true;
            case R.id.menu_supp:
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        mDb.cryptoDAO().deleteAll();

                    }
                });
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onFinishEditDialog(Crypto crypto, int position) {
        if (position > -1) {
            AppExecutors.getInstance().diskIO().execute(new Runnable() {
                @Override
                public void run() {
                    mDb.cryptoDAO().updateCrypto(crypto);
                }
            });
        } else {
            AppExecutors.getInstance().diskIO().execute(new Runnable() {
                @Override
                public void run() {
                    mDb.cryptoDAO().insert(crypto);
                }
            });
        }
    }


}