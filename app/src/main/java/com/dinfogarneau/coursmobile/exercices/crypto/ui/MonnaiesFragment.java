package com.dinfogarneau.coursmobile.exercices.crypto.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toolbar;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dinfogarneau.coursmobile.exercices.crypto.R;
import com.dinfogarneau.coursmobile.exercices.crypto.data.AppExecutors;
import com.dinfogarneau.coursmobile.exercices.crypto.data.CryptoRoomDatabase;
import com.dinfogarneau.coursmobile.exercices.crypto.model.Crypto;
import com.dinfogarneau.coursmobile.exercices.crypto.ui.DialogMonnaieFragment;

import java.util.List;

public class MonnaiesFragment extends Fragment implements DialogMonnaieFragment.EditCryptoDialogListener{

    private static final String MONNAIES = "Monnaies";

    private CryptoRoomDatabase mDb;
    private MonnaieViewModel monnaieViewModel;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        getActivity().setTitle(MONNAIES);
        monnaieViewModel = new ViewModelProvider(this).get(MonnaieViewModel.class);
        return inflater.inflate(R.layout.fragment_monnaies, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.bt_pt).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(MonnaiesFragment.this)
                        .navigate(R.id.action_FirstFragment_to_SecondFragment);
            }
        });


        mDb = CryptoRoomDatabase.getDatabase(getActivity());

        RecyclerView recyclerView = view.findViewById(R.id.rv_monnaies);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
        MonnaiesListAdapter monnaiesListAdapter = new MonnaiesListAdapter();
        recyclerView.setAdapter(monnaiesListAdapter);

        monnaiesListAdapter.setOnClickListener(new MonnaiesListAdapter.onItemClickListener() {
            @Override
            public void ajouterPortefeuille(Crypto crypto) {
                crypto.setPosseder(true);
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        mDb.cryptoDAO().updateCrypto(crypto);
                    }
                });
            }

            @Override
            public void supprimerMonnaie(Crypto crypto) {
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        mDb.cryptoDAO().delete(crypto);
                    }
                });
            }

            @Override
            public void editerMonnaie(int position) {
                DialogMonnaieFragment newFragment = new DialogMonnaieFragment(monnaiesListAdapter.getMonnaiesList().get(position), position);
                newFragment.show(getActivity().getSupportFragmentManager(), "Modifier crypto");
            }
        });

        monnaieViewModel.getCryptos().observe(getViewLifecycleOwner(), new Observer<List<Crypto>>() {
            @Override
            public void onChanged(List<Crypto> cryptos) {
                monnaiesListAdapter.setMonnaiesList(cryptos);
                monnaiesListAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public void onFinishEditDialog(Crypto crypto1, int position) {
        if (position > -1) {
            AppExecutors.getInstance().diskIO().execute(new Runnable() {
                @Override
                public void run() {
                    List<Crypto> cryptos = mDb.cryptoDAO().getAllMonnaiesCrypto();
                    Crypto crypto2 = cryptos.get(position);
                    crypto2.setPosseder(crypto1.isPosseder());
                    crypto2.setNom(crypto1.getNom());
                    crypto2.setPrix(crypto1.getPrix());
                    crypto2.setQuantite(crypto1.getQuantite());
                    crypto2.setSymbole(crypto1.getSymbole());
                    mDb.cryptoDAO().updateCrypto(crypto2);

                }
            });
        } else {
            AppExecutors.getInstance().diskIO().execute(new Runnable() {
                @Override
                public void run() {
                    mDb.cryptoDAO().insert(crypto1);
                }
            });
        }
    }
}